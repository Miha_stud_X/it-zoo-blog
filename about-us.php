<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>IT Zoo community blog</title>
	<link rel="icon" href="img/core-img/logo_icon.ico">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="circle-preloader">
            <img src="img/core-img/logo_blog.png" alt="">
            <div class="itzoo-preloader">
                <span></span>
            </div>
        </div>
    </div>

    <header class="header-area">
		<div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex align-items-center justify-content-between">
                            <div class="logo-hide_small_win">
							<a href="index.php" class="nav-brand"><img src="img/core-img/logo_blog.png" alt=""></a>
							</div>
							<!-- Поиск -->
                            <div class="search-form">
                                <form action="#" method="get">
                                    <input type="search" name="search" class="form-control" placeholder="Что-то интересно?">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="itzoo-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Меню -->
                    <nav class="classy-navbar" id="itzooNav">
						
                        <!-- Логотип в сплывающей шапке -->
                        <a href="index.php" class="nav-brand"><img src="img/core-img/logo_blog.png" alt=""></a>

                        <!--Кнопка раскрытия меню(узкое окно)-->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Меню всплывающее -->
                        <div class="classy-menu">

                            <!-- Навигационное меню -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Главная</a></li>
                                    <li><a href="#">Страницы</a>
                                        <ul class="dropdown">
                                            <li><a href="index.php">Главная</a></li>
                                            <li><a href='postes.php?id="0"'>Статьи</a></li>
                                            <li><a href="about-us.php">О нас</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Категории</a>
                                        <div class="megamenu">
                                            <?php
												$link = mysqli_connect("localhost", "mysql", "mysql", "itzoo-blog") or die("Ошибка " . mysqli_error($link));
												$query ="SELECT id_tag, name FROM tags;";
												$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
												if($result)
												{
													$rows = mysqli_num_rows($result);
													echo'<ul class="single-mega cn-col-2">';
													for ($i = 0 ; $i < ($rows/2) ;++$i)
													{
														$row = mysqli_fetch_object($result);
														$s_link = "\"postes.php?id=".$row->id_tag."\"";
														echo'<li><a href='.$s_link.'>- '.$row->name.'</a></li>';
													}
													echo'</ul><ul class="single-mega cn-col-2">';
													for ($i = ($rows/2) ; $i < $rows ;++$i)
													{
														$row = mysqli_fetch_object($result);
														$s_link = "\"postes.php?id=".$row->id_tag."\"";
														echo'<li><a href='.$s_link.'>- '.$row->name.'</a></li>';
													}
													echo'</ul>';
												}
											?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

	
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="img/core-img/breadcrumb-line.png" alt="">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i> Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">О нас</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    
    <div class="about-us-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <div class="section-heading">
                        <h2>Немого о нас</h2>
                        <p>Мы - команда молодых разработчиков, которые хотят изменить мир</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <div class="about-content">
                        <img src="img/about-img/about_us_1.jpg" alt="">
                        <p>Этот сайт – часть нашего эксперимента по созданию IT сообщества, 
						участники которого хотят поделиться своим опытом и знаниями с другими людьми.</p>
                        <p>Наш блог не только об IT технологиях и программировании, прежде всего он о командной работе и современном образовании.
						 Мы хотим, чтобы при помощи нашего сообщества программисты, дизайнеры, предприниматели, 
						 экономисты и многие другие смогли найти для себя команду специалистов из разных областей для совместной работы.</p>
                        <blockquote>
                            <h5>“IT Зоопарк - это место, где вместе могут ужиться самые разные и диковинные виды айтишников.”</h5>
                        </blockquote>
						<p>Мы против коммерциализации всего и вся, мы хотим, чтобы знания были доступны каждому.</p>
						<blockquote>
                            <h5>“Опен сорс заставляет меня чувствовать себя мазохистом. Я люблю его, однако часто работа с ним – боль.” (c) Мой</h5>
                        </blockquote>
                        <p>На данный момент наш основной состав - 4 неравнодушных разработчика:</p>
						
						<div class="row">
							<div class="col-12" >
								<div class="team-slides owl-carousel">
									<div class="single-team-slide">
										<img src="img/about-img/about_us_4_1.jpg" alt="">
										<a href="https://vk.com/mihailxn">Медведь</a>
									</div>

									<div class="single-team-slide">
										<img src="img/about-img/about_us_4_2.jpg" alt="">
										<a href="https://vk.com/kislovs">Ёжик</a>
									</div>
									
									<div class="single-team-slide">
										<img src="img/about-img/about_us_4_3.jpg" alt="">
										<a href="https://vk.com/id70129023">Орёл</a>
									</div>

									<div class="single-team-slide">
										<img src="img/about-img/about_us_4_4.jpg" alt="">
										<a href="https://vk.com/vuil_smit">Лемур</a>
									</div>

								</div>
							</div>
						</div>
                        
                        <p>Данный сайт разработал студент факультета АВТФ <a href="https://vk.com/mihailxn">Хнюнин Михаил</a>
						в рамках расчётно-графической работы по дисциплине «информационным сети». Прошу строго не критиковать, я только начал свой путь Web-разработчика.</p>
						<img src="img/about-img/about_us_3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<footer class="footer-area">
        <div class="container">
			<img src="img/core-img/breadcrumb-line.png" alt="">
            <div class="copywrite-text">
                <p>IT Zoo</p>
            </div>
        </div>
    </footer>


    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>