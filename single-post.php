<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>IT Zoo community blog</title>
	<link rel="icon" href="img/core-img/logo_icon.ico">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="circle-preloader">
            <img src="img/core-img/logo_blog.png" alt="">
            <div class="itzoo-preloader">
                <span></span>
            </div>
        </div>
    </div>

    <header class="header-area">
		<div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex align-items-center justify-content-between">
                            <div class="logo-hide_small_win">
							<a href="index.php" class="nav-brand"><img src="img/core-img/logo_blog.png" alt=""></a>
							</div>
							<!-- Поиск -->
                            <div class="search-form">
                                <form action="#" method="get">
                                    <input type="search" name="search" class="form-control" placeholder="Что-то интересно?">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="itzoo-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Меню -->
                    <nav class="classy-navbar" id="itzooNav">
						
                        <!-- Логотип в сплывающей шапке -->
                        <a href="index.php" class="nav-brand"><img src="img/core-img/logo_blog.png" alt=""></a>

                        <!--Кнопка раскрытия меню(узкое окно)-->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Меню всплывающее -->
                        <div class="classy-menu">

                            <!-- Навигационное меню -->
                            <div class="classynav">
                                <ul> 
									<li><a href="index.php">Главная</a></li>
                                    <li><a href="#">Страницы</a>
                                        <ul class="dropdown">
                                            <li><a href="index.php">Главная</a></li>
                                            <li><a href='postes.php?id="0"'>Статьти</a></li>
                                            <li><a href="about-us.php">О нас</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Категории</a>
                                        <div class="megamenu">
                                            <?php
												$link = mysqli_connect("localhost", "mysql", "mysql", "itzoo-blog") or die("Ошибка " . mysqli_error($link));
												$query ="SELECT id_tag, name FROM tags;";
												$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
												if($result)
												{
													$rows = mysqli_num_rows($result);
													echo'<ul class="single-mega cn-col-2">';
													for ($i = 0 ; $i < ($rows/2) ;++$i)
													{
														$row = mysqli_fetch_object($result);
														$s_link = "\"postes.php?id=".$row->id_tag."\"";
														echo'<li><a href='.$s_link.'>- '.$row->name.'</a></li>';
													}
													echo'</ul><ul class="single-mega cn-col-2">';
													for ($i = ($rows/2) ; $i < $rows ;++$i)
													{
														$row = mysqli_fetch_object($result);
														$s_link = "\"postes.php?id=".$row->id_tag."\"";
														echo'<li><a href='.$s_link.'>- '.$row->name.'</a></li>';
													}
													echo'</ul>';
												}
											?>
                                        </div>
                                    </li>
                                    <li><a href="about-us.php">О нас</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

	<div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="img/core-img/breadcrumb-line.png" alt="">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i> Главная</a></li>
                            <li class="breadcrumb-item"><a href='postes.php?id="0"'>Статьи</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Пост</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- Контент статьи-->
    <section class="blog-content-area section-padding-0-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="blog-posts-area">
                        <!-- Статья -->
                        <?php
							$id_postes = $_GET['id'];
							$link = mysqli_connect("localhost", "mysql", "mysql", "itzoo-blog") or die("Ошибка " . mysqli_error($link));
							$query = "SELECT p.id_post, 
											p.posts_date,
											TRIM(CONCAT(p.posts_topic_1, ': ', p.posts_topic_2)) AS Title, 
											a.niknames_link,
											a.nikname,
											p.posts_picture_1,
											p.posts_text
											FROM posts p
											INNER JOIN niknames a ON p.id_nikname = a.id_nikname
											WHERE p.id_post = ".$id_postes.";";
							$query_tag = "SELECT t.name, s.id_tag
											FROM post_tags s
											INNER JOIN tags t ON t.id_tag = s.id_tag
											WHERE s.id_post = ".$id_postes.";";
							$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
							if($result)
							{
								$row = mysqli_fetch_object($result);
								echo'<div class="single-post-details-area">';
									echo'<div class="post-thumbnail mb-30">';
										echo'<img src="'.$row->posts_picture_1.'" alt="">';
									echo'</div>';
									echo'<div class="post-content">';
										echo'<p class="post-date">.'.$row->posts_date.'</p>';
										echo'<h4 class="post-title"><h4>'.$row->Title.'</h4>';
										echo'<div class="post-meta"><a href="'.$row->niknames_link.'"><span>by</span>'.$row->nikname.'</a></div>';
										echo $row->posts_text;
									echo'</div>';
								echo'</div>';
							}
							echo'<div class="post-tags-share d-flex justify-content-between align-items-center">';
								#тэги
								$result_tag = mysqli_query($link, $query_tag) or die("Ошибка " . mysqli_error($link));
								$rows = mysqli_num_rows($result_tag);
								for ($i = 0 ; $i < $rows ; ++$i)
								{
									$row_tag = mysqli_fetch_object($result_tag);
									$s_link = "\"postes.php?id=".$row_tag->id_tag."\"";
									if($result_tag){
										echo'<ol class="popular-tags d-flex flex-wrap">';
											echo'<li><a href='.$s_link.'>'.$row_tag->name.'</a></li>';
										echo'</ol>';
									}
								}
							echo'</div>';
						?>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<footer class="footer-area">
        <div class="container">
			<img src="img/core-img/breadcrumb-line.png" alt="">
            <div class="copywrite-text">
                <p>IT Zoo</p>
            </div>
        </div>
    </footer>
	
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>
</html>