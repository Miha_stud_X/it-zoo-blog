(function ($) {
    'use strict';

    var browserWindow = $(window);

    browserWindow.on('load', function () {
        $('.preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    });

    if ($.fn.classyNav) {
        $('#itzooNav').classyNav();
    }

    if ($.fn.owlCarousel) {
        var teamSlides = $('.team-slides');
        
        teamSlides.owlCarousel({
            items: 4,
            margin: 10,
            loop: true,
            nav: false,
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 600,
            responsive: {
                0: {
                    items: 2
                },
                480: {
                    items: 3
                },
                768: {
                    items: 4
                }
            }
        });
    }
	
	if ($.fn.scrollUp) {
        browserWindow.scrollUp({
            scrollSpeed: 1500,
            scrollText: '<i class="fa fa-angle-up"></i>'
        });
    }
	
    if ($.fn.sticky) {
        $(".itzoo-main-menu").sticky({
            topSpacing: 0
        });
    }
	
})(jQuery);